const { request } = require('@imooc-cli-dev-zpp/request');

module.exports = function () {
    return request({
        url: '/project/template'
    });
}
