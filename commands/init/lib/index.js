'use strict';

const Command = require('@imooc-cli-dev-zpp/command');
const log = require('@imooc-cli-dev-zpp/log');
const Package = require('@imooc-cli-dev-zpp/package');
const { spinnerStart, sleep, exec, execSync } = require('@imooc-cli-dev-zpp/utils');
const fs = require('fs');
const fsExtra = require('fs-extra');
const inquirer = require('inquirer');
const path = require("path");
const semver = require('semver');
const userHome = require('user-home');
const getProjectTemplate = require('./getProjectTemplate');

const TYPE_PROJECT = "project";
const TYPE_COMPONENT = "component";
const TEMPLATE_TYPE_NORMAL = "normal";
const TEMPLATE_TYPE_CUSTOM = "custom";
const WHITE_COMMAND = [ 'npm', 'cnpm' ];

class InitCommand extends Command {
    constructor(argv) {
        super(argv);
    }

    init() {
        this.projectName = this._argv[0] || '';
        this.force = !!this._cmd._optionValues.force;
        log.verbose('projectName', this.projectName);
        log.verbose('force', this.force);
    }

    async exec() {
        try {
            /**
             * 1、准备阶段
             * 2、下载模板
             * 3、安装模板
             * */
            const projectInfo = await this.prepare();
            if(projectInfo) {
                log.verbose('projectInfo', projectInfo);
                this.projectInfo = projectInfo;
                /**
                 * 下载模板
                 * */
                await this.downloadTemplate(); // 下载模板
                /**
                 * 安装模板
                 * */
                await this.installTemplate();
            }
        } catch (e) {
            log.error(e.message);
        }
    }

    // 下载模板
    async downloadTemplate() {
        const { projectTemplate } = this.projectInfo;
        const templateInfo = this.template.find(item => item.npmName === projectTemplate);
        const targetPath = path.resolve(userHome, ".imooc-cli-dev-zpp", "template");
        const storePath = path.resolve(userHome, ".imooc-cli-dev-zpp", "template", "node_modules");
        const { npmName, version } = templateInfo;
        this.templateInfo = templateInfo;
        const templateNpm = new Package({
            targetPath,
            storePath,
            packageName: npmName,
            packageVersion: version
        });

        if(!await templateNpm.exists()) {
            const spinner = spinnerStart("正在下载模板......");
            await sleep();
            try {
                // 如果不存在就执行安装操作
                await templateNpm.install();
            } catch (e) {
                log.error("下载模板失败");
                throw e;
            } finally {
                spinner.stop(true);
                if(await templateNpm.exists()) {
                    log.success("下载模板成功");
                    this.templateNpm = templateNpm;
                }
            }
        } else {
            const spinner = spinnerStart("正在更新模板......");
            await sleep();
            try {
                await templateNpm.update();
            } catch (e) {
                log.error("更新模板失败");
                throw e;
            } finally {
                spinner.stop(true);
                if(await templateNpm.exists()) {
                    log.success("更新模板成功");
                    this.templateNpm = templateNpm;
                }
            }
        }
        // 1、通过项目模板API获取项目模板信息
        // 1.1 通过egg.js搭建一套后端系统
        // 1.2 通过npm存储项目模板
        // 1.3 将npm模板信息存储在mongodb中
        // 1.4 通过egg.js获取mongodb中存储的数据

    }

    // 安装模板
    async installTemplate() {
        if(this.templateInfo) {
            if(!this.templateInfo.type) {
                this.templateInfo.type = TEMPLATE_TYPE_NORMAL;
            }

            if(this.templateInfo.type === TEMPLATE_TYPE_NORMAL) {
                /**
                 * 标准安装
                 * */
                await this.installNormalTemplate();
            } else if(this.templateInfo.type === TEMPLATE_TYPE_CUSTOM){
                /**
                 * 自定义安装
                 * */
                await this.installCustomTemplate();
            } else {
                throw Error("无法识别项目模板类型！");
            }
        } else {
            throw Error("项目模板信息不存在！");
        }
    }

    // 安装标准模板
    async installNormalTemplate() {
        log.verbose('templateNpm', this.templateNpm);
        // 拷贝模板代码到当前目录
        let spinner = spinnerStart('正在安装模板....');
        await sleep();
        try {
            const templatePath = path.resolve(this.templateNpm.cacheFilePath, 'template'); //找到当前目录下的template的路径
            const targetPath = process.cwd(); // 获取当前目录
            fsExtra.ensureDirSync(templatePath);
            fsExtra.ensureDirSync(targetPath);
            fsExtra.copySync(templatePath, targetPath); // 拷贝模板到目标目录
        } catch (e) {
            throw e;
        } finally {
            spinner.stop(true);
            log.success('模板安装成功');
        }
        // 依赖安装
        const { installCommand, startCommand } = this.templateInfo;
        this.execCommand(installCommand, '执行安装过程失败！');
        // 启动执行命令
        this.execCommand(startCommand, '执行依赖安装过程失败！')
    }

    async execCommand(command, errorMsg) {
        let execResult;
        if(command) {
            const cmdArray = command.split(' ');
            const cmd = this.checkCommand(cmdArray[0]); // 命令
            if(!cmd) {
                throw Error(`命令${command}不存在。`);
            }
            const args = cmdArray.slice(1); // 命令参数
            execResult = await execSync(cmd, args, {
                stdio: 'inherit',
                cwd: process.cwd(),

            });
            if(execResult !== 0) {
                throw Error(errorMsg);
            } else {
                return execResult;
            }
        }
    }

    checkCommand(cmd) {
        if(WHITE_COMMAND.includes(cmd)) {
            return cmd;
        } else {
            return null;
        }
    }

    // 安装自定义模板
    async installCustomTemplate() {
        console.log("安装自定义模板");
    }

    async prepare() {
        /**
         * 0、判断项目模板是否存在
         * 1、判断当前目录是否为空
         * 2、是否启动强制更新
         * 3、选择创建项目或者组件
         * 4、获取项目的基本信息
         *
         * */
        const template = await getProjectTemplate();
        if(!template || template.length === 0) {
            throw new Error("项目模板不存在");
        }
        this.template = template;
        const currentPath = process.cwd();
        if (!this.isCwdEmpty(currentPath)) {
            let ifContinue = false;
            if(!this.force) {
                // 询问是否继续创建
                ifContinue = (await inquirer.prompt({
                    type: 'confirm',
                    name: 'ifContinue',
                    default: false,
                    message: '当前文件夹不为空，是否继续创建项目？'

                })).ifContinue;
                if(!ifContinue) {
                    return ;
                }
            }
            if(ifContinue || this.force) {
                // 给用户做二次确认
                const { confirmDelete } = await inquirer.prompt({
                    type: 'confirm',
                    name: 'confirmDelete',
                    default: false,
                    message: '是否确认清空当前目录下的文件？'
                });
                if(confirmDelete) {
                    // 清空当前目录
                    fsExtra.emptyDirSync(currentPath);
                }
            }
        }
        return this.getProjectInfo();
    }

    // 获取项目基本信息
    async getProjectInfo() {
        let projectInfo = {};
        const { type } = await inquirer.prompt({
            type: 'list',
            name: 'type',
            message: "请选择初始化类型",
            default: TYPE_PROJECT,
            choices: [
                {
                    name: "项目",
                    value: TYPE_PROJECT
                },
                {
                    name: "组件",
                    value: TYPE_COMPONENT
                }
            ]
        });
        if(type === TYPE_PROJECT) {
            // 获取项目基本信息
            const project = await inquirer.prompt([
                {
                    type: 'input',
                    message: '请输入项目的名称：',
                    name: "projectName",
                    default: '',
                    validate: function(v) {
                        const done = this.async();
                        setTimeout(function() {
                            // 1.首字符必须为英文字符
                            // 2.尾字符必须为英文或数字，不能为字符
                            // 3.字符仅允许"-_"
                            if (!/^[a-zA-Z]+([-][a-zA-Z][a-zA-Z0-9]*|[_][a-zA-Z][a-zA-Z0-9]*|[a-zA-Z0-9])*$/.test(v)) {
                                done('请输入合法的项目名称');
                                return;
                            }
                            done(null, true);
                        }, 0);
                    },
                    filter: function (v) {
                        return v;
                    }
                },
                {
                    type: 'input',
                    name: 'projectVersion',
                    message: '请输入项目版本号：',
                    default: '1.0.0',
                    validate: function (v) {
                        return !!semver.valid(v);
                    },
                    filter: function (v) {
                        if(!!semver.valid(v)) {
                            return semver.valid(v);
                        } else {
                            return v;
                        }
                    }
                },
                {
                    type: "list",
                    name: "projectTemplate",
                    message: "请选择项目模板",
                    choices: this.createTemplateChoice()
                }
            ]);
            projectInfo = {
                type,
                ...project
            };
        } else if(type === TYPE_COMPONENT) {

        }

        // 生成className
        if(projectInfo.projectName) {
            projectInfo.className = require('kebab-case')(projectInfo.projectName).replace(/^-/, '');
        }
        return projectInfo;
    }

    isCwdEmpty(currentPath) {
        let fileList = fs.readdirSync(currentPath); // 读取当前目录下的文件
        // 文件过滤的逻辑
        fileList = fileList.filter(file => !file.startsWith('.') && ['node_modules'].indexOf(file) < 0);
        return !fileList || !fileList.length > 0;
    }

    createTemplateChoice() {
        return this.template.map(item => ({
            name: item.name,
            value: item.npmName
        }));
    }
}

function init(argv) {
    // console.log("init", projectName, cmdObj, process.env.CLI_TARGET_PATH);
    return new InitCommand(argv);
}

module.exports = init;
module.exports.InitCommand = InitCommand;
