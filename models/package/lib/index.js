'use strict';

const path = require('path');
const pathExists = require('path-exists').sync; // path-exists库用于检测一个路径是否存在
const fse = require('fs-extra');
const { isObject } = require('@imooc-cli-dev-zpp/utils');
const formatPath = require('@imooc-cli-dev-zpp/format-path');
const npminstall = require('npminstall');
const { getDefaultRegistry, getNpmLatestVersion } = require('@imooc-cli-dev-zpp/get-npm-info');
const fs = require("fs");
const pkgDir = require('pkg-dir').sync;

class Package {
    constructor(options) {
        if(!options) {
            throw new Error('Package的options参数不能为空！');
        }
        if(!isObject(options)) {
            throw new Error('Package的options参数必须是对象！');
        }
        // package路径
        this.targetPath = options.targetPath;
        // package的存储路径，当package从远程下载到本地之后需要在本地进行存储
        this.storePath = options.storePath;
        // package的name
        this.packageName = options.packageName;
        // package的version
        this.packageVersion = options.packageVersion;
        // package的缓存目录前缀
        this.cacheFilePathPrefix = this.packageName.replace('/', '_');
    }

    async prepare() {
        // 当缓存路径字符串存在但是缓存路径不存在的时候
        if(this.storePath && !pathExists(this.storePath)) {
            fse.mkdirsSync(this.storePath); // 当缓存路径不存在的时候就创建缓存路径
        }

        if(this.packageVersion === 'latest') {
            this.packageVersion = await getNpmLatestVersion(this.packageName);
        }
    }

    // 生成指定版本号模块的路径
    getSpecificCacheFilePath(packageVersion) {
        return path.resolve(this.storePath, `_${this.cacheFilePathPrefix}@${packageVersion}@${this.packageName}`);
    }

    get cacheFilePath() {
        return path.resolve(this.storePath, `_${this.cacheFilePathPrefix}@${this.packageVersion}@${this.packageName}`);
    }

    // 判断当前package是否存在
    async exists() {
        // 当this.storePath存在的时候属于缓存模式
        if(this.storePath) {
            await this.prepare();
            return pathExists(this.cacheFilePath)
        } else {
            // 当处于非缓存模式的时候直接通过path-exists判断targetPath是否存在即可
            return pathExists(this.targetPath);
        }
    }

    // 安装package
    async install() {
        await this.prepare();
        return npminstall({
            root: this.targetPath,
            storeDir: this.storePath,
            registry: getDefaultRegistry(true),
            pkgs: [{
                name: this.packageName,
                version: this.packageVersion
            }]
        });
    }

    // update更新package
    async update() {
        await this.prepare();
        // 1、获取最新的npm模块版本号
        const latestPackageVersion = await getNpmLatestVersion(this.packageName);
        // 2、查询最新版本对应的路径是否存在
        const latestFilePath = this.getSpecificCacheFilePath(latestPackageVersion);
        // 3、如果不存在的话直接安装最新版本
        if(!pathExists(latestFilePath)) {
            await npminstall({
                root: this.targetPath,
                storeDir: this.storePath,
                registry: getDefaultRegistry(true),
                pkgs: [{
                    name: this.packageName,
                    version: latestPackageVersion
                }]
            });
            this.packageVersion = latestPackageVersion;
        }
    }

    // 获取入口文件的路径
    getRootFilePath() {

        function _getRootFile(targetPath) {
            // 1、获取package.json所在的目录
            const dir = pkgDir(targetPath);
            if(dir) {
                const pkgFile = require(path.resolve(dir, 'package.json'));
                // 查找main对应的入口路径
                if(pkgFile && pkgFile.main) {
                    // 路径兼容MacOS和Windows系统
                    return formatPath(path.resolve(dir, pkgFile.main));
                }
            } else {
                return null;
            }
        }

        if(this.storePath) {
            return _getRootFile(this.cacheFilePath);
        } else {
            return _getRootFile(this.targetPath);
        }
    }
}

module.exports = Package;
