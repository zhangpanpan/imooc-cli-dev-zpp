'use strict';

const semver = require('semver');
const colors = require('colors/safe');
const log = require('@imooc-cli-dev-zpp/log');
const LOWEST_NODE_VERSION = "12.0.0";

class Command {
    constructor(argv) {
        if(!argv) {
            throw new Error("命令参数不能为空！");
        }
        if(!Array.isArray(argv)) {
            throw new Error("命令参数必须是数组！");
        }
        if(argv.length < 1) {
            throw new Error("命令参数列表不能为空！")
        }
        this._argv = argv;
        let runner = new Promise(((resolve, reject) => {
            let chain = Promise.resolve();
            chain = chain.then(() => this.checkNodeVersion()); // 检查Node版本
            chain = chain.then(() => this.initArgs()); // 初始化命令参数
            chain = chain.then(() => this.init()); // 执行用户自己的init方法
            chain = chain.then(() => this.exec()); // 执行用户自定义的exec方法
            chain.catch(err => {
               log.error(err.message);
            });
        }));
    }

    init() {
        throw new Error("init必须实现");
    }

    exec() {
        throw new Error("exec必须实现");
    }

    checkNodeVersion() {
        const currentNodeVersion = process.version;
        const lowestNodeVersion = LOWEST_NODE_VERSION;

        if(!semver.gte(currentNodeVersion, lowestNodeVersion)) {
            throw new Error(colors.red(`imooc-cli-dev-zpp 需要安装 v${lowestNodeVersion} 以上版本的Node.js。`));
        }
    }

    // 参数初始化操作
    initArgs() {
        this._cmd = this._argv[this._argv.length - 1];
        this._argv = this._argv.slice(0, this._argv.length - 1);
    }
}

module.exports = Command;
