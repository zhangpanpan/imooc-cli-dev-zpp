#! /usr/bin/env node

const importLocal = require('import-local');

if(importLocal(__filename)) {
    /**
     * 传入的__filename存在的时候就是加载本地文件的情况
     * */
    require('npmlog').info('cli', '正在使用imooc-cli-dev-zpp本地版本');
} else {
    require('../lib')(process.argv.slice(2));
}
