'use strict';

module.exports = core;
const path = require('path');
const commander = require('commander');
const semver = require('semver');
const colors = require('colors/safe');
const userHome = require('user-home');
const pathExists = require('path-exists').sync;
const log = require('@imooc-cli-dev-zpp/log');
const pkg = require('../package.json');
const constant = require('./const.js');
const exec = require('@imooc-cli-dev-zpp/exec');

let args, config;

const program = new commander.Command();

async function core() {
    try {
        await prepare();
        registerCommand();
    } catch (e) {
        if(program.debug) {
            console.log(e);
        }
    }
}

// 注册命令
function registerCommand() {
    program
        .name(Object.keys(pkg.bin)[0])
        .usage('<command> [option]')
        .option('-d, --debug', '是否开启调试模式', false) // 增加一个全局命令属性
        .option('-tp, --targetPath <targetPath>', '是否指定本地文件调试路径', '')
        .version(pkg.version);

    // 注册init命令，用于项目的初始化
    program
        .command('init [projectName]')
        .option('-f, --force', '是否强制初始化项目', false)
        .action(exec)

    program
        .on('option:targetPath', () => {
            process.env.CLI_TARGET_PATH = program.opts().targetPath;
        });

    program.on('option:debug', () => {
        if(process.debug) {
            process.env.LOG_LEVEL = 'verbose';
        } else {
            process.env.LOG_LEVEL = 'info';
        }
    });
    log.level = process.env.LOG_LEVEL;

    // 处理未命中命令的逻辑，当用户输入了不存在的命令时会执行这个逻辑
    program.on('command:*', (cmdObj) => {
        const availableCommands = program.commands.map(cmd => cmd.name());
        console.log(colors.red("未知的命令：" + cmdObj[0]));
        if(availableCommands.length > 0) {
            console.log("可用的命令：" + JSON.stringify(availableCommands));
        }
    });

    program.parse(process.argv);

    // 当用户没有输入命令的时候给用户打印出帮助文档
    if(program.args && program.args.length < 1) {
        program.outputHelp();
        console.log();
    }
}

async function prepare() {
    checkUserHome();
    checkPkgVersion();
    checkRoot();
    checkEnv();
    await checkGlobalUpdate();
}

// 检查全局更新
async function checkGlobalUpdate() {
    const currentVersion = pkg.version;
    const npmName = pkg.name;
    const { getNpmSemverVersions } = require('@imooc-cli-dev-zpp/get-npm-info');
    const latestVersion = await getNpmSemverVersions(currentVersion, npmName, '');
    if(latestVersion && semver.gt(latestVersion, currentVersion)) {
        log.warn('更新提示', colors.yellow(`请手动更新 ${npmName}， 当前版本：${currentVersion}，最新版本：${latestVersion}`));
    }
}

// 检查环境变量
function checkEnv() {
    /**
     * 通过dotenv库检查项目中的.env配置文件的配置
     * */
    const dotEnv = require('dotenv');
    const dotEnvPath = path.resolve(userHome, '.env')
    if(pathExists(dotEnvPath)) {
        config = dotEnv.config({
            path: dotEnvPath
        });
    } else {
        // 如果在用户的主目录下没有.env文件，就创建一个默认的.env文件
        createDefaultConfig();
    }
}

function createDefaultConfig() {
    const cliConfig = {
        home: userHome,
    };
    if(process.env.CLI_HOME) {
        cliConfig['cliHome'] = path.join(userHome, process.env.CLI_HOME);
    } else {
        cliConfig['cliHome'] = path.join(userHome, constant.DEFAULT_CLI_HOME);
    }
    process.env.CLI_HOME_PATH = cliConfig.cliHome;
    return cliConfig;
}

// 检查传入的参数
// function checkInputArgs() {
//     const minimist = require('minimist');
//     args = minimist(process.argv.slice(2));
//     checkArgs(args);
// }

function checkArgs() {
    /**
     * 判断是否为debug模式
     * */
    if(args.debug){
        process.env.LOG_LEVEL = "verbose";
    } else {
        process.env.LOG_LEVEL = "info";
    }
    log.level = process.env.LOG_LEVEL;
}

// 检查用户目录
function checkUserHome() {
    if(!userHome || !pathExists(userHome)) {
        throw new Error(colors.red("当前用户主目录不存在！"));
    }
}

// 检查是否以root权限运行
function checkRoot() {
    const rootCheck = require('root-check');
    // 通过第三方库检测当前是否以root权限运行
    rootCheck();
}

// 检查版本号
function checkPkgVersion() {
    log.notice('cli', pkg.version);
}
