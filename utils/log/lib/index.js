'use strict';

// module.exports = index;

const log = require('npmlog');

log.level = process.env.LOG_LEVEL ? process.env.LOG_LEVEL : 'info';
log.addLevel('success', 2000, {
    fg: 'green',
    blod: true
});

// function index() {
//     log.info('cli', 'test');
// }

module.exports = log;
