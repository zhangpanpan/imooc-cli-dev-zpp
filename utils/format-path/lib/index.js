'use strict';

const path = require('path');

/**
 * 针对不同的操作系统做路径兼容处理
 * */
function formatPath(p) {
    if(p && typeof p === 'string') {
         // sep是路径分隔符
        if(path.sep === '/') {
            return p; // 如果是MacOS或者Linux就直接返回原路径
        } else {
            return p.replace(/\\/g, '/'); // 如果是Windows就将反斜杠替换成斜杠
        }
    }
    return p;
}

module.exports = formatPath;
