'use strict';

const axios = require('axios');
const urlJoin = require('url-join');
const semver = require('semver');

function getNpmInfo(npmName, registry) {
    if(!npmName) {
        return null;
    }
    const registryUrl = registry || getDefaultRegistry();
    const npmInfoUrl = urlJoin(registryUrl, npmName);
    return axios.get(npmInfoUrl).then(response => {
        if(response.status === 200) {
            return response.data;
        } else {
            return null;
        }
    }).catch(error => {
        return new Promise.reject(error);
    })
}

async function getNpmVersions(npmName, registry) {
    const data = await getNpmInfo(npmName, registry);
    if(data) {
        return Object.keys(data.versions);
    } else {
        return [];
    }
}

async function getNpmSemverVersions(baseVersion, npmName, registry) {
    const versionList = await getNpmVersions(npmName, registry);
    const newVersions = getSemverVersions(baseVersion, versionList);
    if(newVersions && newVersions.length > 0) {
        return newVersions[0];
    } else {
        return '';
    }
}

function getSemverVersions(baseVersion, versionList) {
    return versionList.filter(version => semver.satisfies(version, `>${baseVersion}`)).sort((a, b) => semver.gt(b, a));
}

function getDefaultRegistry(isOriginal = false) {
    return isOriginal ? 'https://registry.npmjs.org' : 'https://registry.npm.taobao.org';
}

async function getNpmLatestVersion(npmName, registry) {
    let versions = await getNpmVersions(npmName, registry);
    if(versions) {
        versions = versions.sort((a, b) => semver.gt(b, a));
        return versions[0]; // 排序之后返回第一个最新的版本号
    } else {
        return null;
    }
}

module.exports = {
    getNpmInfo,
    getNpmVersions,
    getNpmSemverVersions,
    getDefaultRegistry,
    getNpmLatestVersion
};
